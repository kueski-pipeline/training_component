from setuptools import setup, find_packages

setup(
    name='kueski-training',
    version='1.0.0',
    packages=find_packages(),
    url='',
    license='',
    author='',
    author_email='',
    description='',
    install_requires=[
        "kfp==1.4.0"
    ]
)
