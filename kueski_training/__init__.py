from kfp import dsl
from kfp.dsl import ContainerOp

from kueski_training.configure import setup_resources, add_environments
from kueski_training.constants import DEFAULT_SPARK_MASTER, STARTUP_COMMAND, IMAGE_NAME,\
    IMAGE_TAG, CPU_LIMIT, CPU_REQUEST, MEMORY_LIMIT, DISPLAY_NAME, MEMORY_REQUEST


@dsl.component
def make_kueski_training(
        source_glob: str = "", validation_source_glob: str = "", model_output_file: str = "",
        spark_master: str = DEFAULT_SPARK_MASTER, min_area_under_roc: float = 0.0, min_area_under_pr: float = 0.0,

        command: str = STARTUP_COMMAND, image_name: str = IMAGE_NAME, image_tag: str = IMAGE_TAG,
        display_name: str = DISPLAY_NAME, memory_request: int = MEMORY_REQUEST,
        memory_limit: int = MEMORY_LIMIT, cpu_request: int = CPU_REQUEST, cpu_limit: int = CPU_LIMIT):

    image = f"{image_name}:{image_tag}"
    operator = ContainerOp(
        image=image,
        name="main",
        arguments=[
            "--source-glob", source_glob,
            "--validation-source-glob", validation_source_glob,
            "--model-output-file", model_output_file,
            "--min-area-under-roc", min_area_under_roc,
            "--min-area-under-pr", min_area_under_pr,
            "--spark-master", spark_master
        ]
    )

    operator = operator.set_display_name(display_name)
    operator = setup_resources(operator, memory_request, memory_limit, cpu_request, cpu_limit)
    operator = add_environments(operator)
    operator.execution_options.caching_strategy.max_cache_staleness = "P0D"

    return operator
